package com.crazyarcade;

public class Launcher {
    
    public static void main(String[] args) {
        Game game = new Game("Crazy Arcade", 400, 400);
        game.start();
    }
}