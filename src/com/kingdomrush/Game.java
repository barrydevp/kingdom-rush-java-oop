package com.crazyarcade;

// import java.awt.Graphics;
// import java.awt.image.BufferStrategy;
// import java.awt.Color;

import java.awt.*;
import java.awt.image.*;

public class Game implements Runnable {
    private Display display;
    public int width, height;
    String title;

    private Thread thread;
    private boolean running = false;

    private BufferStrategy bs;
    private Graphics g;

    private BufferedImage testImage;

    public Game(String title, int width, int height) {
        this.width = width;
        this.height = height;
        this.title = title;
    }

    private void init() {
        display = new Display(title, width, height);
        testImage = ImageLoader.loadImage("/drawable/bazzi.png");
    }

    private void tick() {

    }

    private void render() {
        bs = display.getCanvas().getBufferStrategy();
        if(bs == null) {
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();

        // clear screen
        g.clearRect(0, 0, width, height);

        // draw

        g.drawImage(testImage, 20, 20, null);
        // g.setColor(Color.RED);
        // g.drawRect(10, 50, 50, 70);
        // g.fillRect(0, 0, 30, 30);

        // end draw

        bs.show();
        g.dispose();

    }

    public void run() {

        init();

        while(running) {
            tick();
            render();
        }

        stop();
    }

    public synchronized void start() {
        if(running) return;
        // else
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if(!running) return;
        // else
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}